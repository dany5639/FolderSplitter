﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FolderSplitter
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        public static int FilesCount = 0;
        public static IEnumerable<string> EnumerateFiles;
        public string folder = "";
        public bool firstRun = false;

        private void button1_Click(object sender, EventArgs e)
        {
            if (!firstRun)
                folderBrowserDialog1.SelectedPath = $"{Directory.GetCurrentDirectory()}";

            firstRun = true;

            folderBrowserDialog1.ShowDialog();
            folder = folderBrowserDialog1.SelectedPath;

            if (folder == "")
                return;

            EnumerateFiles = Directory.EnumerateFiles(folder);

            FilesCount = EnumerateFiles.Count();
            label1.Text = $"Found {FilesCount} files.";
        }

        private void button_move_click(object sender, EventArgs e)
        {
            var filesPerFolder = numericUpDown1.Value;

            if (filesPerFolder > FilesCount)
            {
                label1.Text = $"ERROR: Cannot have less than one file per folder.";
                return;
            }

            label1.Text = "Moving files...";

            var folderCount = Math.Ceiling(FilesCount / filesPerFolder);

            for (int i = 0; i < folderCount; i++)
            {
                var newFolder = $"{folder}\\{folder.Split("\\".ToCharArray()).Last()}_{i}";
                Directory.CreateDirectory(newFolder);

                for (int j = 0; j < filesPerFolder; j++)
                {
                    try
                    {
                        var newPath = $"{newFolder}\\{EnumerateFiles.First().Split("\\".ToCharArray()).Last()}";

                        File.Move(EnumerateFiles.First(), newPath);
                    }
                    catch
                    {

                    }
                }
            }

            label1.Text = $"Done. Created {folderCount} folders.";
        }
    }
}
